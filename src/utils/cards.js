import * as cardStatus from '@constants/cardStatus'

const cardsTypes = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'jack', 'queen', 'king']
const cardsSuits = ['heart', 'diamond', 'club', 'spade']

const randomInt = (max) => {
  let rand = -0.5 + Math.random() * (max + 1)
  return Math.round(rand)
}

export const generateCards = (pairsNumber) => {
  let cardsList = []
  let alreadyExistsCards = {}
  let prevCard = null
  for (let i = 0; i < pairsNumber; i++) {
    let cardName
    do {
      cardName = cardsSuits[randomInt(cardsSuits.length - 1)] + '_' + cardsTypes[randomInt(cardsTypes.length - 1)]
    } while (alreadyExistsCards[cardName])
    alreadyExistsCards[cardName] = true
    let card = {
      name: cardName,
      status: cardStatus.BACK }
    cardsList.push(card)
    if (prevCard) {
      cardsList.push(prevCard)
    }
    prevCard = Object.assign({}, card)
  }
  cardsList.push(prevCard)
  cardsList.sort((a, b) => Math.random() - 0.5)
  return cardsList
}
