import CardsField from '@components/CardsField'

const generatedCardsList = [
  {"name":"club_jack","status":"BACK"},
  {"name":"club_jack","status":"BACK"},
  {"name":"diamond_5","status":"BACK"},
  {"name":"diamond_5","status":"BACK"}
]

describe('CardsField',()=>{
  it('check snapshot', () => {
    const cardsFieldWrapper = shallow(
        <CardsField cards={generatedCardsList} onCardClick={()=>{}} />
    );
    expect(cardsFieldWrapper).toMatchSnapshot()
  })
})
