import Timer from '@components/Timer'
import * as timerStatus from '@constants/timerStatus'

jest.useFakeTimers();

describe('Timer',()=>{
  it('check snapshot with RESET status', () => {
    const timerWrapper = shallow(
        <Timer status={timerStatus.RESET} />
    );
    expect(timerWrapper).toMatchSnapshot();
  })

  it('should start timer after status will be changed to ON', () => {
    const timerWrapper = shallow(
        <Timer status={timerStatus.RESET} />
    );
    timerWrapper.setProps({ status:timerStatus.ON })
    expect(setInterval).toHaveBeenCalled()
  })

  it('should stop timer after status will be changed to OFF', () => {
    const timerWrapper = shallow(
        <Timer status={timerStatus.ON} />
    );
    timerWrapper.instance().stop = jest.fn();
    timerWrapper.setProps({ status:timerStatus.OFF })
    expect(timerWrapper.instance().stop).toHaveBeenCalled()
  })

  it('should reset timer after status will be changed to RESET', () => {
    const timerWrapper = shallow(
        <Timer status={timerStatus.ON} />
    );
    timerWrapper.instance().reset = jest.fn();
    timerWrapper.setProps({ status:timerStatus.RESET })
    expect(timerWrapper.instance().reset).toHaveBeenCalled()
  })
})
