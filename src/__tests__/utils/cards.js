import { generateCards} from '@utils/cards'

describe('@utils/cards',()=>{
  it('generateCards should generate not duplicated number pairs of cards', () => {
    const pairsNumber = 18
    const cards = generateCards(pairsNumber)
    expect(cards.length).toBe(pairsNumber*2)
    let pairsNotDuplicates = true
    for (let i=0; i<cards.lenght; i++){
      let count = 0
      for (let j=0; j<cards.lenght; j++) {
        if (cards[i].name===cards[j].name) {
          count++
        }
      }
      if (count !== 2) {
        pairsNotDuplicates = false
        break
      }
    }
    expect(pairsNotDuplicates).toBe(true)
  })
})
