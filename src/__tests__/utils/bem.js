import Bem from '@utils/bem'

const style = {
  block: 'block',
  block_mod1: 'block_mod1',
  block_mod2: 'block_mod2',
  block__element: 'block__element',
  block__element_mod1: 'block__element_mod1',
  block__element_mod2: 'block__element_mod2',
}

describe('@utils/bem',()=>{
  it('check bem rules', () => {
    const b = Bem(style)
    expect(b('block')).toBe('block')
    expect(b('block',{mod1:false})).toBe('block')
    expect(b('block',{mod1:true})).toBe('block block_mod1')
    expect(b('block',{mod1:true, mod2:false})).toBe('block block_mod1')
    expect(b('block',{mod1:true, mod2:true})).toBe('block block_mod1 block_mod2')
    expect(b('block','element')).toBe('block__element')
    expect(b('block','element',{mod1:false})).toBe('block__element')
    expect(b('block','element',{mod1:true})).toBe('block__element block__element_mod1')
    expect(b('block','element',{mod1:true, mod2:false})).toBe('block__element block__element_mod1')
    expect(b('block','element',{mod1:true, mod2:true})).toBe('block__element block__element_mod1 block__element_mod2')
  })
})
