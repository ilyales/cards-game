import React from 'react'

import style from './style/index.css'
import Bem from '@utils/bem'
import * as cardStatus from '@constants/cardStatus'
import * as timerStatus from '@constants/timerStatus'
import { generateCards } from '@utils/cards'
import CardsField from '@components/CardsField'
import Timer from '@components/Timer'

const b = Bem(style)

export default class Cards extends React.Component {
  constructor (props) {
    super(props)
    this.pairsNumber = 18
    this.openedPairsCount = 0
    this.initFirstSecondCards()
    this.state = {
      cards: generateCards(this.pairsNumber),
      score: 0,
      isFinish: false,
      timerStatus: timerStatus.RESET
    }
  }

  initFirstSecondCards = () => {
    this.firstCardName = null
    this.firstCardIdx = null
    this.isSecondCardOpen = false
  }

  handleCardClick = (idx) => {
    const card = this.state.cards[idx]
    if (card.status === cardStatus.HIDDEN ||
        idx === this.firstCardIdx ||
        this.isSecondCardOpen
    ) return
    this.setState((state) => (
      { cards: this.changeCardsStatus(this.state.cards, idx, cardStatus.FRONT) }
    ))
    if (!this.firstCardName) {
      this.firstCardName = card.name
      this.firstCardIdx = idx
      this.setState((state) => ({ score: state.score + 1 }))
      if (this.state.timerStatus === timerStatus.RESET) {
        this.setState({ timerStatus: timerStatus.ON })
      }
    } else {
      this.isSecondCardOpen = true
      if (this.firstCardName === card.name) {
        this.openedPairsCount++
        if (this.openedPairsCount === this.pairsNumber) {
          this.setState({
            isFinish: true,
            timerStatus: timerStatus.OFF
          })
        }
        setTimeout(() => {
          this.setState((state) => ({
            cards: this.changeCardsStatus(this.state.cards, [this.firstCardIdx, idx], cardStatus.HIDDEN)
          }))
          this.initFirstSecondCards()
        }, 200)
      } else {
        setTimeout(() => {
          this.setState((state) => ({
            cards: this.changeCardsStatus(this.state.cards, [this.firstCardIdx, idx], cardStatus.BACK)
          }))
          this.initFirstSecondCards()
        }, 1500)
      }
    }
  }

  changeCardsStatus = (cards, idx, status) => {
    let idxArray
    if (Array.isArray(idx)) {
      idxArray = idx
    } else {
      idxArray = [idx]
    }
    const changedCards = idxArray.reduce((acc, cur, i) => {
      acc[cur] = Object.assign({}, cards[cur], { status })
      return acc
    }, {})
    return Object.assign(
      [...cards],
      changedCards
    )
  }

  startNewGame = () => {
    this.openedPairsCount = 0
    this.initFirstSecondCards()
    this.setState({
      cards: generateCards(this.pairsNumber),
      score: 0,
      isFinish: false,
      timerStatus: timerStatus.RESET
    })
  }

  render () {
    const {
      cards,
      score,
      isFinish,
      timerStatus
    } = this.state
    return <div className={b('cards')}>
      <div className={b('cards', 'field')}>
        <CardsField
          cards={cards}
          onCardClick={this.handleCardClick}
        />
      </div>
      <div className={b('cards', 'info')}>
        <div className={b('cards', 'score')} >
          Количество ходов:&nbsp;
          <span className={b('cards', 'score-value')}>{score}</span>
        </div>
        <Timer status={timerStatus} />
        { isFinish &&
          <div className={b('cards', 'finish')}>
            <div className={b('cards', 'finish-text')}>Игра завершена</div>
          </div>
        }
        <div className={b('cards', 'start-btn')} onClick={this.startNewGame}>Начать заново</div>
      </div>
    </div>
  }
}
