import React from 'react'
import PropTypes from 'prop-types'

import style from './style/index.css'
import Bem from '@utils/bem'
import * as timerStatus from '@constants/timerStatus'

const b = Bem(style)

export default class Timer extends React.PureComponent {
  constructor (props) {
    super(props)
    this.state = {
      value: this.formatTime(0)
    }
    this.seconds = 0
  }

  formatTime = (allSeconds) => {
    let hours = Math.round(allSeconds / 36000)
    let minutes = Math.round(allSeconds / 60)
    let seconds = Math.round(allSeconds % 60)
    if (hours < 10) {
      hours = '0' + hours
    }
    if (minutes < 10) {
      minutes = '0' + minutes
    }
    if (seconds < 10) {
      seconds = '0' + seconds
    }
    return `${hours} : ${minutes} : ${seconds}`
  }

  start = () => {
    this.setState({
      value: this.formatTime(this.seconds)
    })
    this.timerId = setInterval(() => {
      this.seconds++
      this.setState({
        value: this.formatTime(this.seconds)
      })
    }, 1000)
  }

  stop = () => {
    clearInterval(this.timerId)
  }

  reset = () => {
    this.seconds = 0
    this.setState({
      value: this.formatTime(this.seconds)
    })
  }

  componentDidUpdate (prevProps, prevState, snapshot) {
    if (prevProps.status !== this.props.status) {
      switch (this.props.status) {
        case timerStatus.ON:
          this.start()
          break
        case timerStatus.OFF:
          this.stop()
          break
        case timerStatus.RESET:
          this.stop()
          this.reset()
          break
      }
    }
  }

  render () {
    return <div className={b('timer')}>
      Время: {this.state.value}
    </div>
  }
}

Timer.propTypes = {
  status: PropTypes.string.isRequired
}
