module.exports = {
  plugins: {
    // Формируем правильный css
    'postcss-mixins': {},
    'postcss-each': {},
    'postcss-conditionals': {},
    // Включаем линтер
    'stylelint': {},
    // Поcтобработка для совместимости
    'postcss-preset-env': {
      stage: 0,
      'autoprefixer': {
        grid: true,
      },
    },
    // Normalize
    'postcss-normalize': {
      forceImport: true
    },
    // Минифицируем
    'cssnano': {},
  }
}
